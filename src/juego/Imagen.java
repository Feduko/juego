package juego;

import javax.swing.ImageIcon;

import java.awt.Image;

// Clase que carga una imagen usando ImageIcon y la pasa a Image en el constructor 
public class Imagen {
    private Image imagen;

    // constructor de imagen , recibe la ruta, utilizammos imageIcon para subir la imagen y image para poder usarla.
    public Imagen(String ruta) {
        // Cargar la imagen desde un archivo
    	ImageIcon icono = new ImageIcon(getClass().getResource(ruta));
    	// Verificar si la imagen se cargó correctamente
        if (icono.getImageLoadStatus() == java.awt.MediaTracker.COMPLETE) {
            System.out.println("La imagen se cargó correctamente.");
        } else {
            System.out.println("No se pudo cargar la imagen.");
        }
        this.imagen = icono.getImage();
    }

    public Image getImagen() {
        return this.imagen;
    }
}
