package juego;


import java.util.Random;



import entorno.Entorno;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	//declaro objeto fondo
	private Fondo fondo;
	//declaro objeto bloque
	private Bloques bloque;

	// Variables y métodos propios de cada grupo
	// ...

	Juego() {
		Random rand = new Random();
		// Inicializa el objeto entorno
		// Inicializa el objeto fondo
		this.entorno = new Entorno(this, " Super Elizabeth Sis, Volcano Edition - Grupo ... - v1", 800, 600);
		this.fondo = new Fondo("../imagenes/lava.jpg",400,300,0,1.5);
		this.bloque= new Bloques(400,300,45,45);
		this.entorno.iniciar();
	} 

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y por lo
	 * tanto es el método más importante de esta clase. Aquí se debe actualizar el
	 * estado interno del juego para simular el paso del tiempo (ver el enunciado
	 * del TP para mayor detalle).
	 */
	public void tick() {
		
		//usamos metodos
		
		this.fondo.dibujarFondo(this.entorno);
		this.bloque.dibujar(this.entorno);
		
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}
}
