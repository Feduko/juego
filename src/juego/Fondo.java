package juego;

import java.awt.Image;


import entorno.Entorno;

public class Fondo {
	// declaramos la variable imagen , como IMAGEN.
	private Imagen imagen;
	private double x;
	private double y;
	private double angulo;
	private double escala;

	
	public Fondo(String rutadelaimagen, double x, double y, double angulo, double escala) {
		this.imagen = new Imagen(rutadelaimagen);
		this.x = x;
		this.y = y;
		this.angulo = angulo;
		this.escala = escala;
	}


	public void dibujarFondo(Entorno entorno) {
		
		entorno.dibujarImagen(this.imagen.getImagen(), this.x, this.y, this.angulo, this.escala);
		
	}

}
